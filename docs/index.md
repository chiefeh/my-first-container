# My First Container - Welcome to Chiefeh's Blog

Hello World! This is my first container with the contents of an MkDocs site.

I've decided to start recording my technology things so I can reference them
in future when I forget things and for the benefit of others who are in a
similar position. I've chosen MkDocs as the way to convert Markdown
files into nice to look at websites with search.
See the about section to see how I built this (and is acting as my first project
to learn about this new world!)

First up is Infrastructure as code (IaC). This will depend on Terraform, Ansible,
containers and Continuous Integration/Continuous Delivery (CI/CD) to build the
Infrastructure that will run applications. I am not a developer so a lot of this
will be new to me, along with figuring out some new apps such as Atom.io,
Git/GitHub and so on.

